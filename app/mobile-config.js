App.info({
    id: 'com.gafundraising.fiftyniner.mobile',
    name: 'BLITZ',
    description: 'Mobile application for GAO Fiftyniner.',
    author: 'Matthew Wheatley',
    email: 'mwheatley@swgao.com',
    version: '2.5.3'
});

App.icons({
    // iOS
    'iphone_2x': 'resources/ios/icons/Icon-60@2x.png', // 120x120
    'iphone_3x': 'resources/ios/icons/Icon-60@3x.png', // 180x180
    'ipad': 'resources/ios/icons/Icon-76.png', // 76x76
    'ipad_2x': 'resources/ios/icons/Icon-76@2x.png', // 152x152
    'ipad_pro': 'resources/ios/icons/Icon-167.png', // 167x167
    'ios_settings': 'resources/ios/icons/Icon-29.png', // 29x29
    'ios_settings_2x': 'resources/ios/icons/Icon-29@2x.png', // 58x58
    'ios_settings_3x': 'resources/ios/icons/Icon-29@3x.png', // 87x87
    'ios_spotlight': 'resources/ios/icons/Icon-40.png', // 40x40
    'ios_spotlight_2x': 'resources/ios/icons/Icon-40@2x.png', // 80x80,
    'ipad_app_legacy': 'resources/ios/icons/Icon-72.png', // 72x72
    'ipad_app_legacy_2x': 'resources/ios/icons/Icon-72@2x.png', // 144x144

    // Android
    'android_mdpi': 'resources/android/icons/ic_launcher_mdpi.png', // 48x48
    'android_hdpi': 'resources/android/icons/ic_launcher_hdpi.png', // 72x72
    'android_xhdpi': 'resources/android/icons/ic_launcher_xhdpi.png', // 96x96
    'android_xxhdpi': 'resources/android/icons/ic_launcher_xxhdpi.png', // 144x144
    'android_xxxhdpi': 'resources/android/icons/ic_launcher_xxxhdpi.png' // 192x192
});
App.setPreference("MARKETING_ICON_FILENAME","marketing_icon_1024x1024.png");
App.setPreference("ICONS_TO_REMOVE","icon-40.png,icon-50.png,icon-50@2x.png,icon.png,icon@2x.png");

App.launchScreens({
    //  // iOS
    'iphone_2x': 'resources/ios/splash/screen-iphone-portrait-2x.png', // 320x480@2x || 640x960
    'iphone5': 'resources/ios/splash/screen-iphone-568h-2x.png', // 320x568@2x || 640x1136
    'iphone6': 'resources/ios/splash/screen-iphone-portrait-667h-2x.png', // 375x667@2x || 750x1334
    'iphone6p_portrait': 'resources/ios/splash/screen-iphone-portrait-736h-3x.png', // 414x736@3x || 1242x2208
    'iphone6p_landscape': 'resources/ios/splash/screen-iphone-landscape-736h-3x.png', // 736x414@3x || 2208x1242

    'ipad_portrait': 'resources/ios/splash/screen-ipad-portrait.png', // 768x1024
    'ipad_portrait_2x': 'resources/ios/splash/screen-ipad-portrait-2x.png', // 768x1024@2x || 1536x2048
    'ipad_landscape': 'resources/ios/splash/screen-ipad-landscape.png', // 1024x768
    'ipad_landscape_2x': 'resources/ios/splash/screen-ipad-landscape-2x.png', // 1024x768@2x || 2048x1536

    // Android
    'android_mdpi_portrait': 'resources/android/splash/screen-mdpi-portrait.png', // 320x480
    'android_mdpi_landscape': 'resources/android/splash/screen-mdpi-landscape.png', // 480x320
    'android_hdpi_portrait': 'resources/android/splash/screen-hdpi-portrait.png', // 480x800
    'android_hdpi_landscape': 'resources/android/splash/screen-hdpi-landscape.png', // 800x480
    'android_xhdpi_portrait': 'resources/android/splash/screen-xhdpi-portrait.png', // 720x1280
    'android_xhdpi_landscape': 'resources/android/splash/screen-xhdpi-landscape.png' // 1280x720
});

App.setPreference('BackupWebStorage', 'local');
App.setPreference('StatusBarOverlaysWebView', 'true');
//App.setPreference('StatusBarBackgroundColor', '#000000');
App.setPreference('loadUrlTimeoutValue', 700000);
App.setPreference('SplashShowOnlyFirstTime', 'false');
App.setPreference('android-targetSdkVersion', '26');

// App.configurePlugin('cordova-plugin-contacts', {
//     CONTACTS_USAGE_DESCRIPTION: ""
// });

App.configurePlugin('cordova-plugin-camera', {
    CAMERA_USAGE_DESCRIPTION: "This application will use your phone's camera to take a selfie for your profile picture.",
    PHOTOLIBRARY_USAGE_DESCRIPTION: "This application will access your photo library to select a photo for your profile picture."
});

// Custom URL Scheme
App.configurePlugin('cordova-plugin-customurlscheme', {
    URL_SCHEME: "mobilegametimegafundraising",
    ANDROID_SCHEME: "https",
    ANDROID_HOST: "mobile.gametime.gafundraising.com"
});

App.configurePlugin('phonegap-plugin-push', {
    SENDER_ID: 540876162888
});

App.appendToConfig(`
    <platform name="android">
        <custom-config-file target="AndroidManifest.xml" parent="/*">
            <uses-feature android:name="android.hardware.camera" android:required="true" xmlns:android="http://schemas.android.com/apk/res/android" />
            <uses-feature android:name="android.hardware.camera.autofocus" xmlns:android="http://schemas.android.com/apk/res/android" />
            <uses-permission android:name="android.permission.CAMERA" xmlns:android="http://schemas.android.com/apk/res/android" />
            <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" xmlns:android="http://schemas.android.com/apk/res/android" />
        </custom-config-file>
    </platform>
    <platform name="ios"> 
        <config-file platform="ios" target="*-Info.plist" parent="UILaunchStoryboardName">
            <string>CDVLaunchScreen</string>
        </config-file>
        <edit-config target="NSContactsUsageDescription" file="*-Info.plist" mode="merge">
            <string>Add a contact from your phone to your Blitz contact list.  A contact's name and phone number will be associated with your user profile and stored in the cloud.</string>
        </edit-config>
      </platform>
`);

App.accessRule('mobilegametimegafundraising://*');
App.accessRule('http://meteor.local');
App.accessRule('http://localhost:12664/');
App.accessRule('http://localhost:3800/*');
App.accessRule('https://localhost:3800/*');
App.accessRule('http://10.0.2.2:3800/*');
App.accessRule('http://10.35.3.197:3800/*');
App.accessRule('http://10.35.2.163:3800/*');
App.accessRule('http://192.168.2.4:3800/*');
App.accessRule('http://10.0.2.2:3880/*');
App.accessRule('http://10.35.3.197:3880/*');
App.accessRule('http://10.35.2.163:3880/*');
App.accessRule('http://192.168.0.100:3880/*');
App.accessRule('*.google.com/*');
App.accessRule('*.googleapis.com/*');
App.accessRule('*.gstatic.com/*');
App.accessRule('https://fifty9min-app.duckdns.org/*');
App.accessRule('https://dev.gametime.gafundraising.com:8080/*');
App.accessRule('https://test.mobile.gametime.gafundraising.com/*');
App.accessRule('https://mobile.gametime.gafundraising.com/*');
App.accessRule('https://dev.gametime.gafundraising.com/*');
App.accessRule('https://test.gametime.gafundraising.com/*');
App.accessRule('https://gametime.gafundraising.com/*');
