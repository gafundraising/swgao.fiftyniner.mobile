import {Constants} from "../../both/Constants";
import {ProgramsCollection} from "../../both/collections/programs.collection";
declare var Roles;

Meteor.publish(Constants.PUBLICATIONS.MANAGED_PROGRAMS, function () {
    var user:Meteor.User = Meteor.users.findOne({_id: this.userId});
    var membershipProgramIds:Array<string> = [];
    if (user && user["roles"]) {
        var programIds:Array<string> = Object.keys(user["roles"]) || [];
        programIds.forEach((programId:string) => {
            if (Roles.userIsInRole(user._id, [Constants.ROLES.ADMIN, Constants.ROLES.PROGRAM_MANAGEMENT], programId)) {
                membershipProgramIds.push(programId);
            }
        });
    }
    return ProgramsCollection.find({
        _id: {$in: membershipProgramIds},
        $or: [{
            "contract.cancelDate": {
                $exists: false
            }
        }, {
            "contract.cancelDate": {
                $eq: Constants.EMPTY_STRING
            }
        }]
    });
});

Meteor.publish(Constants.PUBLICATIONS.PROGRAM_MEMBERSHIPS, function (membershipProgramIds) {
    if (membershipProgramIds && membershipProgramIds.length > 0) {
        return ProgramsCollection.find({
            _id: {$in: membershipProgramIds},
            $or: [{
                "contract.cancelDate": {
                    $exists: false
                }
            }, {
                "contract.cancelDate": {
                    $eq: Constants.EMPTY_STRING
                }
            }]
        });
    }
});