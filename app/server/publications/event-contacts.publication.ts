import {Constants} from "../../both/Constants";
import {EventContactsCollection} from "../../both/collections/event-contacts.collection";
Meteor.publish(Constants.PUBLICATIONS.PARTICIPANT_EVENT_CONTACTS, function (data:{
    participantId:string,
    programIds:Array<string>
}) {
    if (data) {
        if (data.programIds && data.programIds.length > 0) {
            return EventContactsCollection.find({userId: data.participantId, programId: {$in: data.programIds}});
        }
    }
});