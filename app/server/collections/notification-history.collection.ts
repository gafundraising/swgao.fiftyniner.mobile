import {IPushNotification} from "../../both/models/push-notification.model";
export const NotificationHistoryCollection = new Mongo.Collection<IPushNotification>('notification_history');
