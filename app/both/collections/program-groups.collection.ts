import {IProgramGroup} from "../models/program-group.model";
declare var Ground;

export const ProgramGroupsCollection = new Mongo.Collection<IProgramGroup>("program_groups");

if (Meteor.isCordova) {
    Ground.Collection(ProgramGroupsCollection);
}

if (Meteor.isServer) {
    ProgramGroupsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    ProgramGroupsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}
