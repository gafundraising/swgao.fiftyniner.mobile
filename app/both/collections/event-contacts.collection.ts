import {IEventContact} from "../models/event-contact.model";
declare var Ground;

export const EventContactsCollection = new Mongo.Collection<IEventContact>("event_contacts");
export const GroundEventContactsCollection = new Ground.Collection(EventContactsCollection, "event_contacts");

if (Meteor.isServer) {
    EventContactsCollection.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    // EventContactsCollection.deny({
    //     insert: function (userId, doc) {
    //         return false;
    //     },
    //
    //     update: function (userId, doc, fieldNames, modifier) {
    //         return false;
    //     },
    //
    //     remove: function (userId, doc) {
    //         return true;
    //     }
    // });
}
