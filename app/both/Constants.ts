import {IAwardInfo} from "./models/award-info.model";
export class Constants {
    public static EMPTY_STRING = "";

    public static SESSION:any = {
        DEVICE_READY: "deviceready",
        LANGUAGE: "language",
        LOADING: "isLoading",
        PLATFORM_READY: "platformReady",
        TRANSLATIONS_READY: "translationsReady",
        PATH: "path",
        URL_PARAMS: "urlParams",
        INCORRECT_PASSWORD: "incorrectPassword",
        FORGOT_PASSWORD: "forgotPassword",
        CREATE_ACCOUNT: "createAccount",
        RESET_PASSWORD: "resetPassword",
        REGISTERED_ERROR: "registeredError",
        NOT_REGISTERED_ERROR: "notRegisteredError",
        RESET_PASSWORD_ERROR: "resetPasswordError",
        RESET_PASSWORD_ERROR_MESSAGE: "resetPasswordErrorMessage",
        RESET_PASSWORD_TOKEN: "resetPasswordToken",
        WAS_PASSWORD_RESET: "wasPasswordReset",
        EMAIL: "email",
        MOBILE_PHONE: "mobilePhone",
        PROGRAM_ID: "programId",
        INVALID_KICKOFF_CODE: "invalidKickoffCode",
        CONTACT_COMPLETE: "contactComplete",
        PUSH_TOKEN: "pushToken",
        NOTIFICATION_RECEIVED: "notificationReceived",
        PUSH_NOTIFICATION: "notificationPayload",
        IS_IPHONE_X_LAYOUT: "isIPhoneXLayout"
    };

    public static DEVICE:any = {
        IOS: "iOS",
        ANDROID: "Android"
    };

    public static STYLE:any = {
        IOS: "ios",
        MD: "md"
    };

    public static METEOR_ERRORS:any = {
        SIGN_IN: "sign-in",
        ACCOUNT_NOT_FOUND: "account-not-found",
        NO_PASSWORD: "User has no password set",
        USER_NOT_FOUND: "User not found",
        INCORRECT_PASSWORD: "Incorrect password",
        ALREADY_EXISTS: 'already-exists',
        EMAIL_EXISTS: "Email already exists.",
        USERNAME_EXISTS: "Username already exists.",
        TOKEN_EXPIRED: "Token expired",
        FINGERPRINT_NOT_ENABLED: "fingerprint-not-enabled",
        INVALID_REGISTRATION_CODE: "invalid-registration-code",
        PERMISSION_DENIED: "User has denied permission",
        INVALID_KICKOFF_CODE: "invalid-kickoff-code",
        TIMEDOUT: "ETIMEDOUT"
    };

    public static GAO_LOGO:string = "/images/gao_logo.png";
    public static APP_ICON:string = "/images/blitz_icon.png";
    public static APP_LOGO:string = "/images/blitz_logo.png";
    public static ADD_IMAGE_PLACEHOLDER_URI:string = "/images/add_image_camera_photo.png";
    public static IMAGE_URI_PREFIX:string = "data:image/jpeg;base64,";

    public static CIPHER_MODE:any = {
        ENCRYPT: "encrypt",
        DECRYPT: "decrypt"
    };

    public static PUBLICATIONS:any = {
        MANAGED_PROGRAMS: "ManagedPrograms",
        PROGRAM_MEMBERSHIPS: "ProgramMemberships",
        RANKINGS: "Rankings",
        PROGRAM_PARTICIPANTS: "ProgramParticipants",
        MY_LIST_CONTACTS: "MyListContacts",
        PARTICIPANT_EVENT_CONTACTS: "ParticipantEventContacts",
        PROGRAM_GROUPS: "ProgramGroups"
    };

    public static ROLES:any = {
        ADMIN: "administrator",
        MEMBER: "member",
        PROGRAM_MANAGEMENT: "program-management"
    };

    // Linked to language.json keys
    public static EVENT_CONTACT_METHOD:any = {
        CALL: "call",
        TEXT: "text",
        EMAIL: "email"
    };

    public static EVENT_CONTACT_STATUS:any = {
        CALLED: "Called",
        LEFT_MESSAGE: "Left Voice Message",
        SENT_TEXT: "Sent Text Message",
        DECLINED: "Declined Commitment",
        RECEIVED_COMMITMENT: "Received Commitment",
        SENT_ORDER_CONFIRMATION: "Sent Order Confirmation",
        PAID: "Paid"
    };

    public static ROUTES:any = {
        RESET_PASSWORD: "resetPassword"
    };

    public static COMPETITION_TYPE:any = {
        INDIVIDUAL: "individual",
        GROUP: "group"
    };

    public static ENVIRONMENT:any = {
        DEVELOPMENT: "DEVELOPMENT",
        TEST: "TEST",
        PRODUCTION: "PRODUCTION"
    };

    public static PUSH_NOTIFICATION_ACTIONS:any = {
        MESSAGE: "message",
        AWARD: "award"
    };

    public static AWARD_KEYS:any = {
        REGISTRATION: "registration",
        CONTACT_LIST: "contactList",
        COMMUNICATION: "communication",
        SALES: "sales"
    };

    public static AWARD_INFO:any = {
        registration: {
            key: Constants.AWARD_KEYS.REGISTRATION,
            imageUri: "/images/icon_registration_award.png",
            color: "#0B9444"
        },
        contactList: {
            key: Constants.AWARD_KEYS.CONTACT_LIST,
            imageUri: "/images/icon_contact_list_award.png",
            color: "#E32526"
        },
        communication: {
            key: Constants.AWARD_KEYS.COMMUNICATION,
            imageUri: "/images/icon_communication_award.png",
            color: "#222C6A"
        },
        sales: {
            key: Constants.AWARD_KEYS.SALES,
            imageUri: "/images/icon_sales_goal_award.png",
            color: "#F36C24"
        }
    };

    public static DIVISION_CODE:any = {
        GA: "20",
        GS: "21",
        QSPCA: "40",
        BDCCA: "41",
        BDCUS: "42"
    };
    
    public static PAYMENT_LINK_PARAMS:any = {
        GTID: "gtid=",
        ONLINE_ID: "?PCOnlineID="
    };
}