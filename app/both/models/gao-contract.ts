export interface IGAOContract {
    contractNumber?:number, // code to register in the 59-min app
    contractId?:number,
    shopUrl?:string, // URI to supportoursale.com or gaschoolstore.com with {OnlineStudentID}
    startDate?:string,  // ISO Date string
    cancelDate?:string, // ISO Date string,
    group?:{
        number?:number,
        name?:string
    },
    salesRep?:IGAOPartner,
    sponsor?:IGAOPartner,
    product?:{
        label?:string, // single product, TextType: SupportOurSaleProductLabel
        imageUrl?:string, // URI to single product image, ImageType: SupportOurSaleProduct
        price?:number
    },
    divisionCode?:string,
    languageCode?:string,
    profitPercent?:number
}

export interface IGAOPartner {
    number?:number,
    name?:string,
    phone?:string,
    email?:string
}