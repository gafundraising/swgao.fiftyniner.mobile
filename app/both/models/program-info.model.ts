import * as moment from 'moment';
import {IGAOContract} from "./gao-contract";
import {Constants} from "../Constants";

export interface IProgramInfo {
    _id?:string,
    userId?:string,
    imageUri?:string,
    name?:string,
    eventPoints?:IEventPoints,
    duration?:number,
    contactListGoal?:number,
    salesGoal?:number,
    salesGoalTotal?:number,
    scripts?:IProgramScripts,
    startTime?:string,
    contract?:IGAOContract,
    eventDate?:string,
    created?:string,
    competitionType?:string
}

export class ProgramInfo implements IProgramInfo {
    public _id:string;
    public userId:string;
    public imageUri:string;
    public name:string = null;
    public eventPoints:EventPoints = new EventPoints({
        call:1,
        text:2,
        email:1,
        paid:10,
        commitment:5,
        contactListGoal:15,
        contactGoal:15,
        registration:5,
        salesGoalAward: 25
    });
    public duration:number = 59;
    public contactListGoal:number = 30;
    public salesGoal:number = 15;
    public salesGoalTotal:number = 0;
    public scripts:IProgramScripts = {
        "callToAction": "Contact everyone on your list!",
        "call": "Hi {{contactName}},\n\nWe're doing a fundraiser for my ________ team at school. My goal is to sell {{salesGoal}} items in {{duration}} minutes. We are selling {{productName}}.  Would you be willing to support me?",
        "text": "We are doing a fundraiser for my ________ team at school. My goal is to sell {{salesGoal}} items in {{duration}} minutes. We are selling {{productName}}.  Would you be willing to support me?\n\nText or call me at {{myPhoneNumber}}.\n\nThanks, {{me}}",
        "email": "Hi {{contactName}},\n\nWe are doing a fundraiser for my ________ team at school. My goal is to sell {{salesGoal}} items in {{duration}} minutes. We are selling {{productName}}.  Would you be willing to support me?\n\nText or call me at {{myPhoneNumber}}.\n\nThanks,\n\n{{me}}",
        "orderConfirmation": "Pay online: {{paymentLink}}\n\nThank you for pledging to purchase {{quantity}} {{productName}} for ${{totalCost}}.\n\nPlease use the link provided to make an online payment.\n\nThanks for supporting me and my team! -- {{me}}"
    };
    public startTime:string;
    public contract:IGAOContract = {};
    public eventDate:string;
    public created:string = moment().toISOString();
    public competitionType:string = Constants.COMPETITION_TYPE.INDIVIDUAL;

    constructor(programInfoJson?:IProgramInfo) {
        if (programInfoJson) {
            this._id = programInfoJson._id;
            this.userId = programInfoJson.userId;
            this.imageUri = programInfoJson.imageUri;
            if (programInfoJson.hasOwnProperty('name')) {
                this.name = programInfoJson.name;
            }
            if (programInfoJson.hasOwnProperty('eventPoints')) {
                this.eventPoints = new EventPoints(programInfoJson.eventPoints);
            }
            if (programInfoJson.hasOwnProperty('duration')) {
                this.duration = programInfoJson.duration;
            }
            if (programInfoJson.hasOwnProperty('contactListGoal')) {
                this.contactListGoal = programInfoJson.contactListGoal;
            }
            if (programInfoJson.hasOwnProperty('salesGoal')) {
                this.salesGoal = programInfoJson.salesGoal;
            }
            if (programInfoJson.hasOwnProperty('salesGoalTotal')) {
                this.salesGoalTotal = programInfoJson.salesGoalTotal;
            }
            if (programInfoJson.hasOwnProperty('scripts')) {
                this.scripts = programInfoJson.scripts;
            }
            if (programInfoJson.hasOwnProperty('startTime')) {
                this.startTime = programInfoJson.startTime;
            }
            if (programInfoJson.hasOwnProperty("contract")) {
                this.contract = programInfoJson.contract;
            }
            if (programInfoJson.hasOwnProperty("eventDate")) {
                this.eventDate = programInfoJson.eventDate;
            }
            if (programInfoJson.hasOwnProperty("created")) {
                this.created = programInfoJson.created;
            }
            if (programInfoJson.hasOwnProperty("competitionType")) {
                this.competitionType = programInfoJson.competitionType;
            }
        }
    }
}

export interface IEventPoints {
    _id?:string,
    userId?:string,
    name?:string,
    call?:number,
    text?:number,
    email?:number,
    paid?:number,
    commitment?:number,
    contactListGoal?:number,
    contactGoal?:number,
    registration?:number,
    salesGoalAward?:number,
    updated?:string
}

export class EventPoints implements IEventPoints {
    public _id:string;
    public userId:string;
    public name:string;
    public call:number;
    public text:number;
    public email:number;
    public paid:number;
    public commitment:number;
    public contactListGoal:number;
    public contactGoal:number;
    public registration:number;
    public salesGoalAward:number;
    public updated:string;

    constructor(eventPointsJson?:IEventPoints) {
        if (eventPointsJson) {
            if (eventPointsJson.hasOwnProperty("_id")) {
                this._id = eventPointsJson._id;
            }
            if (eventPointsJson.hasOwnProperty("userId")) {
                this.userId = eventPointsJson.userId;
            }
            if (eventPointsJson.hasOwnProperty("name")) {
                this.name = eventPointsJson.name;
            }
            if (eventPointsJson.hasOwnProperty("call")) {
                this.call = eventPointsJson.call;
            }
            if (eventPointsJson.hasOwnProperty("text")) {
                this.text = eventPointsJson.text;
            }
            if (eventPointsJson.hasOwnProperty("email")) {
                this.email = eventPointsJson.email;
            }
            if (eventPointsJson.hasOwnProperty("paid")) {
                this.paid = eventPointsJson.paid;
            }
            if (eventPointsJson.hasOwnProperty("commitment")) {
                this.commitment = eventPointsJson.commitment;
            }
            if (eventPointsJson.hasOwnProperty("contactListGoal")) {
                this.contactListGoal = eventPointsJson.contactListGoal;
            }
            if (eventPointsJson.hasOwnProperty("contactGoal")) {
                this.contactGoal = eventPointsJson.contactGoal;
            }
            if (eventPointsJson.hasOwnProperty("registration")) {
                this.registration = eventPointsJson.registration;
            }
            if (eventPointsJson.hasOwnProperty("salesGoalAward")) {
                this.salesGoalAward = eventPointsJson.salesGoalAward;
            }
            if (eventPointsJson.hasOwnProperty("updated")) {
                this.updated = eventPointsJson.updated;
            }
        }
    }
}

export interface IProgramScripts {
    _id?:string,
    userId?:string,
    name?:string,
    callToAction?:string,
    call?:string,
    text?:string,
    email?:string,
    orderConfirmation?:string
}

export class ProgramScripts implements IProgramScripts {
    public _id:string;
    public userId:string;
    public name:string;
    public callToAction:string;
    public call:string;
    public text:string;
    public email:string;
    public orderConfirmation:string;
    
    constructor(scriptJson:IProgramScripts) {
        if (scriptJson) {
            if (scriptJson.hasOwnProperty("_id")) {
                this._id = scriptJson._id;
            }
            if (scriptJson.hasOwnProperty("userId")) {
                this.userId = scriptJson.userId;
            }
            if (scriptJson.hasOwnProperty("name")) {
                this.name = scriptJson.name;
            }
            if (scriptJson.hasOwnProperty("callToAction")) {
                this.callToAction = scriptJson.callToAction;
            }
            if (scriptJson.hasOwnProperty("call")) {
                this.call = scriptJson.call;
            }
            if (scriptJson.hasOwnProperty("text")) {
                this.text = scriptJson.text;
            }
            if (scriptJson.hasOwnProperty("email")) {
                this.email = scriptJson.email;
            }
            if (scriptJson.hasOwnProperty("orderConfirmation")) {
                this.orderConfirmation = scriptJson.orderConfirmation;
            }
        }
    }
}