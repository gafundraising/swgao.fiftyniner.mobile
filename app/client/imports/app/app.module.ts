import {NgModule, ErrorHandler} from "@angular/core";
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular/es2015';
import {Storage} from '@ionic/storage';
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {TranslateModule, TranslateLoader} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {HttpClientModule, HttpClient} from "@angular/common/http";
import {Constants} from "../../../both/Constants";
import {AppComponent} from "./app.component";
import {HomePage} from "./pages/home/home";
import {LanguageSelectComponent} from "./components/language-select/language-select";
import {AboutPage} from "./pages/about/about";
import {NewPagePage} from "./pages/newpage/newpage";

// Login components
import {LoginPage} from "./pages/account/login/login";
import {LoginCardComponent} from "./pages/account/login/login-card/login-card";
import {CreateAccountCardComponent} from "./pages/account/login/create-account-card/create-account-card";
import {ForgotPasswordCardComponent} from "./pages/account/login/forgot-password-card/forgot-password-card";
import {PasswordResetCardComponent} from "./pages/account/login/password-reset-card/password-reset-card";
import {OauthProviderComponent} from "./pages/account/login/oauth/oauth-provider";

// Account management components
import {AccountMenuPage} from "./pages/account/account-menu/account-menu";
import {ChangePasswordPage} from "./pages/account/account-menu/change-password/change-password";
import {EditProfilePage} from "./pages/account/account-menu/edit-profile/edit-profile";
import {AddImageComponent} from "./components/add-image/add-image";
import {FingerprintLoginToggleComponent} from "./components/fingerprint-login-toggle/fingerprint-login-toggle";

// Other pages
import {HomeTabPage} from "./pages/home/tabs/home-tab/home-tab";
import {InfoTabPage} from "./pages/home/tabs/info-tab/info-tab";
import {FollowUpsTabPage} from "./pages/home/tabs/follow-ups-tab/follow-ups-tab";
import {RankingsTabPage} from "./pages/home/tabs/rankings-tab/rankings-tab";
import {ProgramManagementPage} from "./pages/program-management/program-management";
import {ProgramRegistrationPage} from "./pages/program-registration/program-registration";
import {LandingPage} from "./pages/landing/landing";
import {ProgramSelectPage} from "./pages/program-select/program-select";
import {ContactsTabPage} from "./pages/home/tabs/contacts-tab/contacts-tab";
import {ContactInfoPage} from "./pages/event-contact/contact-info/contact-info";
import {ContactPage} from "./pages/event-contact/contact/contact";
import {MyContactListPage} from "./pages/event-contact/my-contact-list/my-contact-list";
import {PhonePipe} from "./pipes/phone.pipe";
import {OrderCommitmentPage} from "./pages/event-contact/order-commitment/order-commitment";
import {ConfirmRegistrationPage} from "./pages/program-registration/confirm-registration/confirm-registration";
import {MomentPipe} from "./pipes/moment.pipe";
import {NotificationModalPage} from "./modals/notification-modal/notification-modal";
import {AwardsCardComponent} from "./components/awards-card/awards-card";
import {AwardProgressModalPage} from "./modals/award-progress-modal/award-progress-modal";

// Ionic native plugins
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";
import {Device} from "@ionic-native/device";
import {Sim} from "@ionic-native/sim";
import {SMS} from "@ionic-native/sms";
import {Contacts} from "@ionic-native/contacts";
import {EmailComposer} from "@ionic-native/email-composer";
import {Camera} from "@ionic-native/camera";
import {AppRate} from "@ionic-native/app-rate";
import {AndroidPermissions} from "@ionic-native/android-permissions";

//Providers
import {ImageService} from "./utils/ImageService";

@NgModule({
    // Components(Pages), Pipes, Directive
    declarations: [
        AppComponent,
        NewPagePage,
        LandingPage,
        HomePage,
        LanguageSelectComponent,
        AboutPage,
        LoginPage,
        LoginCardComponent,
        CreateAccountCardComponent,
        ForgotPasswordCardComponent,
        PasswordResetCardComponent,
        OauthProviderComponent,
        AccountMenuPage,
        ChangePasswordPage,
        EditProfilePage,
        AddImageComponent,
        FingerprintLoginToggleComponent,
        ProgramManagementPage,
        ProgramRegistrationPage,
        ProgramSelectPage,
        HomeTabPage,
        ContactsTabPage,
        RankingsTabPage,
        FollowUpsTabPage,
        InfoTabPage,
        ContactInfoPage,
        ContactPage,
        MyContactListPage,
        PhonePipe,
        OrderCommitmentPage,
        ConfirmRegistrationPage,
        MomentPipe,
        NotificationModalPage,
        AwardsCardComponent,
        AwardProgressModalPage
    ],
    // Pages
    entryComponents: [
        AppComponent,
        LandingPage,
        HomePage,
        LoginPage,
        AboutPage,
        AccountMenuPage,
        ChangePasswordPage,
        EditProfilePage,
        ProgramManagementPage,
        ProgramRegistrationPage,
        ProgramSelectPage,
        HomeTabPage,
        ContactsTabPage,
        RankingsTabPage,
        FollowUpsTabPage,
        InfoTabPage,
        ContactInfoPage,
        ContactPage,
        MyContactListPage,
        OrderCommitmentPage,
        ConfirmRegistrationPage,
        NotificationModalPage,
        AwardProgressModalPage
    ],
    // Providers
    providers: [
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        },
        ImageService,
        SplashScreen,
        StatusBar,
        Device,
        Sim,
        SMS,
        Contacts,
        EmailComposer,
        Camera,
        AppRate,
        AndroidPermissions
    ],
    // Modules
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        IonicModule.forRoot(AppComponent, {
            //// http://ionicframework.com/docs/v2/api/config/Config/
            //mode: Constants.STYLE.MD,
            //pageTransition: Constants.STYLE.IOS,
            tabsPlacement: 'top',
            tabsHighlight: true,
            swipeBackEnabled: false,
            platforms: {
                ios: {
                    statusbarPadding: true
                }
            }
        }),
    ],
    // Main Component
    bootstrap: [IonicApp]
})
export class AppModule {
    constructor() {

    }
}

export function createTranslateLoader(http:HttpClient) {
    return new TranslateHttpLoader(http, '/i18n/', '.json');
}

