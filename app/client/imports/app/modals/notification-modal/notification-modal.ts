import {Component, OnInit, NgZone} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular/es2015';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {IPushNotification} from "../../../../../both/models/push-notification.model";
import {Constants} from "../../../../../both/Constants";
import {ProgramsCollection} from "../../../../../both/collections/programs.collection";
import {IProgramInfo} from "../../../../../both/models/program-info.model";
@Component({
    selector: "page-notification-modal",
    templateUrl: "notification-modal.html"
})
export class NotificationModalPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public notification:IPushNotification;
    public program:IProgramInfo;
    private appLogo:string = Constants.APP_LOGO;

    constructor(public nav:NavController,
                public params:NavParams,
                public viewCtrl:ViewController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
        }));

        this.notification = this.params.data;
        var programId:string = this.notification.payload.programId;
        this.autorun(() => this.zone.run(() => {
            this.program = ProgramsCollection.findOne({_id: programId});
        }));

    }

    private dismiss():void {
        this.viewCtrl.dismiss();
    }
}