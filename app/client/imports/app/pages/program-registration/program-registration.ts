import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, AlertController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import {Constants} from "../../../../../both/Constants";
import {FormValidator} from "../../utils/FormValidator";
import {ToastMessenger} from "../../utils/ToastMessenger";
import {ProgramInfo} from "../../../../../both/models/program-info.model";
import {ConfirmRegistrationPage} from "./confirm-registration/confirm-registration";
@Component({
    selector: "page-program-registration",
    templateUrl: "program-registration.html"
})
export class ProgramRegistrationPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public formGroup:FormGroup;
    public formControl:{
        kickoffCode:AbstractControl
    };
    public kickoffCode:string = null;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public fb:FormBuilder,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.autorun(() => {
            this.user = Meteor.user();
        });

        this.formGroup = this.fb.group({
            'kickoffCode': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.kickoffCodeValid
            ])]
        });

        this.formControl = {
            kickoffCode: this.formGroup.controls['kickoffCode']
        };
    }
    
    private onSubmit():void {
        var self = this;
        if (self.formGroup.valid) {
            Session.set(Constants.SESSION.LOADING, true);
            Meteor.call('programRegistration', {registrationCode: self.kickoffCode}, (error, result) => {
                Session.set(Constants.SESSION.LOADING, false);
                if (error) {
                    console.log("programRegistration() Error: " + JSON.stringify(error));
                    if (error.error === Constants.METEOR_ERRORS.INVALID_REGISTRATION_CODE) {
                        console.log("KICKOFF_CODE_INVALID");
                        Session.set(Constants.SESSION.INVALID_KICKOFF_CODE, true);
                        self.zone.run(() => {
                            self.formControl.kickoffCode.updateValueAndValidity({onlySelf: true});
                        });
                        new ToastMessenger().toast({
                            type: "error",
                            message: self.translate.instant("page-program-registration.errors.kickoffCode")
                        });
                    } else {
                        let alert = self.alertCtrl.create({
                            title: self.translate.instant("page-program-registration.errors.registration"),
                            message: error.reason || error.message,
                            buttons: [self.translate.instant("general.ok")]
                        });
                        alert.present();
                    }
                } else {
                    console.log("Found program: ", result);
                    var program:ProgramInfo = new ProgramInfo(result);
                    self.nav.push(ConfirmRegistrationPage, program);
                }
            });
        }
    }
}