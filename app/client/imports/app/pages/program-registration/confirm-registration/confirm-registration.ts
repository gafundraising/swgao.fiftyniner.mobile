import {Component, OnInit, NgZone} from '@angular/core';
import {NavController, NavParams, AlertController} from 'ionic-angular/es2015';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {IProgramInfo, ProgramInfo} from "../../../../../../both/models/program-info.model";
import {Constants} from "../../../../../../both/Constants";
import {LandingPage} from "../../landing/landing";

declare var Session;

@Component({
    selector: "page-confirm-registration",
    templateUrl: "confirm-registration.html"
})
export class ConfirmRegistrationPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public program:IProgramInfo;
    public programImageUri:string = Constants.APP_LOGO;

    constructor(public nav:NavController,
                public params:NavParams,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.program = new ProgramInfo(this.params.data);
        if (this.program.imageUri) {
            this.programImageUri = this.program.imageUri;
        }

        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
        }));
    }

    private confirmRegistration():void {
        var self = this;
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call('confirmProgramRegistration', {programId: self.program._id}, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.log("confirmProgramRegistration() Error: " + JSON.stringify(error));
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-program-registration.errors.registration"),
                    message: error.reason || error.message,
                    buttons: [self.translate.instant("general.ok")]
                });
                alert.present();
            } else {
                console.log("confirmProgramRegistration() Result:" + JSON.stringify(result));
                if (!result.success) {
                    let alert = self.alertCtrl.create({
                        title: self.translate.instant("page-program-registration.errors.registration"),
                        message: result.message,
                        buttons: [self.translate.instant("general.ok")]
                    });
                    alert.present();
                } else {
                    Session.setAuth(Constants.SESSION.PROGRAM_ID, result.programId);
                    Meteor.defer(() => {
                        self.zone.run(() => {
                            self.nav.setRoot(LandingPage);
                        });
                    });
                }
            }
        });
    }
}