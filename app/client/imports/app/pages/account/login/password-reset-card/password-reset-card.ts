import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, AlertController} from 'ionic-angular/es2015';
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../../../both/Constants";
import {FormValidator} from "../../../../utils/FormValidator";

declare var SHA256;

@Component({
    selector: "password-reset-card",
    templateUrl: "password-reset-card.html"
})
export class PasswordResetCardComponent extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public passwordResetForm:FormGroup;
    public formControl:{
        // passwordResetCode:AbstractControl,
        password:AbstractControl,
        confirmPassword:AbstractControl
    };
    // public resetPasswordError:boolean = false;
    // public resetPasswordErrorMessage:string;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public fb:FormBuilder,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.passwordResetForm = this.fb.group({
            // 'passwordResetCode': [Constants.EMPTY_STRING, Validators.compose([
            //     Validators.required,
            //     FormValidator.validPasswordResetToken
            // ])],
            'password': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])],
            'confirmPassword': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])]
        }, {validator: FormValidator.matchingFields('mismatchedPasswords', 'password', 'confirmPassword')});
        this.formControl = {
            // passwordResetCode: this.passwordResetForm.controls['passwordResetCode'],
            password: this.passwordResetForm.controls['password'],
            confirmPassword: this.passwordResetForm.controls['confirmPassword']
        };

        // this.autorun(() => this.zone.run(() => {
        //     this.user = Meteor.user();
        //     this.resetPasswordError = Session.get(Constants.SESSION.RESET_PASSWORD_ERROR);
        //     this.resetPasswordErrorMessage = Session.get(Constants.SESSION.RESET_PASSWORD_ERROR_MESSAGE);
        // }));
    }

    public resetPassword(form:{
        // passwordResetCode:string, 
        password:string,
        confirmPassword:string
    }):void {
        var self = this;
        if (this.passwordResetForm.valid) {
            Session.set(Constants.SESSION.LOADING, true);
            Meteor.call("resetPasswordFromSMS", {
                    token: Session.get(Constants.SESSION.RESET_PASSWORD_TOKEN),
                    password: {
                        digest: SHA256(form.password),
                        algorithm: 'sha-256'
                    }
                },
                (error, result) => {
                    Session.set(Constants.SESSION.LOADING, false);
                    if (error) {
                        console.log("Reset Password Error: " + JSON.stringify(error));
                        if (error.reason === Constants.METEOR_ERRORS.TOKEN_EXPIRED) {
                            // Session.set(Constants.SESSION.RESET_PASSWORD_ERROR, true);
                            // Session.set(Constants.SESSION.RESET_PASSWORD_ERROR_MESSAGE,
                            //     self.translate.instant("password-reset-card.errors.passwordResetCode"));
                            // self.formControl.passwordResetCode.updateValueAndValidity({onlySelf: true});
                            let alert = self.alertCtrl.create({
                                title: self.translate.instant("password-reset-card.errors.passwordResetError"),
                                message: self.translate.instant("password-reset-card.errors.passwordResetCode"),
                                buttons: [{
                                    text: self.translate.instant("general.ok"),
                                    handler: () => {
                                        Session.set(Constants.SESSION.RESET_PASSWORD_TOKEN, null);
                                        Session.set(Constants.SESSION.RESET_PASSWORD, false);
                                        Session.set(Constants.SESSION.FORGOT_PASSWORD, true);
                                    }
                                }]
                            });
                            alert.present();
                        } else {
                            let errorMsg = error;
                            if (error.reason) {
                                errorMsg = error.reason;
                            } else if (error.message) {
                                errorMsg = error.message;
                            }

                            let alert = self.alertCtrl.create({
                                title: self.translate.instant("password-reset-card.errors.passwordResetError"),
                                message: errorMsg,
                                buttons: [self.translate.instant("general.ok")]
                            });
                            alert.present();
                        }
                    } else {
                        if (result.success) {
                            console.log("Successfully changed password");
                            Session.set(Constants.SESSION.RESET_PASSWORD_TOKEN, null);
                            Session.set(Constants.SESSION.RESET_PASSWORD, false);
                            Session.set(Constants.SESSION.WAS_PASSWORD_RESET, true);
                            Meteor.loginWithToken(result.loginToken);
                        }
                    }
                }
            );
        }
    }

    public showSignInCard() {
        Session.set(Constants.SESSION.RESET_PASSWORD, !Session.get(Constants.SESSION.RESET_PASSWORD));
    }
}