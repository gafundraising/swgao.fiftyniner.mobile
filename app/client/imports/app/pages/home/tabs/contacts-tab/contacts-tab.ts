import {Component, OnInit} from '@angular/core';
import {NavController, AlertController} from 'ionic-angular/es2015';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {IProgramInfo} from "../../../../../../../both/models/program-info.model";
import {ProgramsCollection} from "../../../../../../../both/collections/programs.collection";
import {Constants} from "../../../../../../../both/Constants";
import {ContactInfoPage} from "../../../event-contact/contact-info/contact-info";
import {MyContactListPage} from "../../../event-contact/my-contact-list/my-contact-list";

@Component({
    selector: "page-contacts-tab",
    templateUrl: "contacts-tab.html"
})
export class ContactsTabPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public program:IProgramInfo;
    public CONTACT_METHOD:any = Constants.EVENT_CONTACT_METHOD;
    public contactButtons:Array<any>;
    
    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.autorun(() => {
            this.user = Meteor.user();
            this.program = ProgramsCollection.findOne({_id: Session.get(Constants.SESSION.PROGRAM_ID)});
        });
        
        this.contactButtons = [{
            icon: "call",
            method: Constants.EVENT_CONTACT_METHOD.CALL
        }, {
            icon: "text",
            method: Constants.EVENT_CONTACT_METHOD.TEXT
        }, {
            icon: "mail",
            method: Constants.EVENT_CONTACT_METHOD.EMAIL
        }];
    }
    
    private getContactInfo(method:string):void {
        this.nav.push(ContactInfoPage, {method: method});
    }
    
    private myContactList():void {
        this.nav.push(MyContactListPage);
    }
}