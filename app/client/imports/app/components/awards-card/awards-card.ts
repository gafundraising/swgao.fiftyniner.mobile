import {Component, OnInit, NgZone, ViewChildren, QueryList, ElementRef, Input} from "@angular/core";
import {ModalController} from 'ionic-angular/es2015';
import {TranslateService} from "@ngx-translate/core";
import {MeteorComponent} from "angular2-meteor";
import {IProgramInfo} from "../../../../../both/models/program-info.model";
import {AwardProgressModalPage} from "../../modals/award-progress-modal/award-progress-modal";
import {IAwardInfo} from "../../../../../both/models/award-info.model";
declare var ProgressBar;
@Component({
    selector: "component-awards-card",
    templateUrl: "awards-card.html"
})
export class AwardsCardComponent extends MeteorComponent implements OnInit {
    @Input() awards:Array<IAwardInfo>;
    @ViewChildren('awardProgressBar') progressBars:QueryList<ElementRef>;
    public user:Meteor.User;
    public program:IProgramInfo;
    // public awards:Array<IAwardInfo> = [];

    constructor(public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => this.zone.run(() => {

        }));
    }

    ngOnChanges() {
        // Defer to ensure progress bar containers loaded in DOM
        Meteor.defer(() => {
            this.zone.run(() => {
                if (this.awards) {
                    this.awards.forEach((award:IAwardInfo) => {
                        if (award.progress < 1) {
                            if (!award.progressBar) {
                                let elementRef:ElementRef = this.progressBars.toArray().find((ref:ElementRef) => {
                                    return ref.nativeElement.id == award.key;
                                });
                                if (elementRef) {
                                    award.progressBar = new ProgressBar.Line(
                                        elementRef.nativeElement,
                                        this.getProgBarConfig(award)
                                    );
                                }
                            }
                            if (award.progressBar) {
                                award.progressBar.animate(award.progress);
                            }
                        }
                    });
                }
            });
        });
    }

    private getProgBarConfig(award:IAwardInfo):any {
        return {
            strokeWidth: 8,
            easing: 'easeInOut',
            duration: 1400,
            color: award.color,
            trailColor: '#aaa',
            trailWidth: 4,
            svgStyle: {width: '50%', height: '100%'}
        };
    }

    private openAwardProgressModal(awardInfo:IAwardInfo):void {
        var modal = this.modalCtrl.create(AwardProgressModalPage, {awardInfo: awardInfo});
        modal.present();
    }
}