//import {PushNotificationHelper} from "../utils/PushNotificationHelper";
import {Constants} from "../../../../both/Constants";
import {PushNotificationHelper} from "./PushNotificationHelper";

declare var device;
declare var navigator;
declare var StatusBar;
declare var window;
declare var hockeyapp;
declare var AppRate;

export class DeviceReady {
    public static onDeviceReady():void {
        console.log("onDeviceReady()");
        Session.set(Constants.SESSION.DEVICE_READY, true);
        // Listen for application resumed event
        document.addEventListener('resume', this.onResume, false);

        Session.set(Constants.SESSION.DEVICE_READY, true);
        navigator.globalization.getPreferredLanguage((language) => {
            console.log('navigator.globalization.getPreferredLanguage(): ' + language.value);
            // if (language.value === 'es-ES' || language.value === 'es-US') {
            //     console.log("Set default language to spanish");
            // }
        }, () => {
            console.log('Error getting language');
        });

        // // StatusBar style
        // if (device.platform === Constants.DEVICE.IOS) {
        //     StatusBar.styleDefault();
        // } else if (device.platform === Constants.DEVICE.ANDROID) {
        //     // StatusBar.style* is not supported on Android devices
        //     //StatusBar.backgroundColorByHexString("#333");
        //
        //     /**
        //      * Supported color names:
        //      * black, darkGray, lightGray, white, gray, red, green, blue, cyan, yellow, magenta, orange, purple, brown
        //      * */
        //     StatusBar.backgroundColorByName("gray");
        // }
        // StatusBar.show();

        PushNotificationHelper.setPushListeners();

        // Change color of recent apps title bar color (Android)
        if (device.platform === Constants.DEVICE.ANDROID) {
            window.plugins.headerColor.tint("#C60817");
        }

        // Hockey App
        let hockeyAppConfig:any = Meteor.settings.public["hockeyApp"];
        let hockeyAppId:string = hockeyAppConfig[device.platform.toLocaleLowerCase()].appId;
        hockeyapp.start(()=> {
            console.log("Hockey App started!");
        }, ()=> {
            console.log("Error starting Hockey App!!!")
        }, hockeyAppId, true);

        // Rate App
        AppRate.preferences = {
            displayAppName: Meteor.settings.public["appName"],
            useLanguage: "en",
            usesUntilPrompt: 3,
            promptAgainForEachNewVersion: true,
            inAppReview: true,
            storeAppURL: {
                ios: '1232380684',
                android: 'market://details?id=com.gafundraising.fiftyniner.mobile'
            },
            callbacks: {
                handleNegativeFeedback: function () {
                    // window.open('mailto:feedback@example.com', '_system');
                },
                onRateDialogShow: function (callback) {
                    // callback(1) // cause immediate click on 'Rate Now' button
                },
                onButtonClicked: function (buttonIndex) {
                    console.log("onButtonClicked -> " + buttonIndex);
                }
            }
        };

        AppRate.promptForRating(false);
    }

    private static onResume() {
        //console.log("Application resumed");
    }
}