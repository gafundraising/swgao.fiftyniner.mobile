import {Constants} from "../../../../both/Constants";
export class PhoneFormatter {
    public static transform(tel:string, args?:any) {
        var country:string;
        var area:string;
        var city:string;
        var number:string;
        var formattedTel:string;

        // remove non digits
        var regex = /^\d*$/;
        if (tel && !regex.test(tel)) {
            tel = tel.replace(/[^0-9]/g, Constants.EMPTY_STRING);
        }

        switch (tel.length) {
            case 7:
                city = tel.slice(0, 3);
                number = tel.slice(3);
                formattedTel = city + " - " + number;
                break;
            case 10:
                area = tel.slice(0, 3);
                city = tel.slice(3, 6);
                number = tel.slice(6);
                formattedTel = "(" + area + ") " + city + " - " + number;
                break;

            case 11:
                country = "+" + tel[0];
                area = tel.slice(1, 4);
                city = tel.slice(4, 7);
                number = tel.slice(7);
                formattedTel = country + " (" + area + ") " + city + " - " + number;
                break;

            default:
                return tel;
        }

        return formattedTel;
    }
}