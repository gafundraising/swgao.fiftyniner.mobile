import {Constants} from "../../both/Constants";
declare var window;

Meteor.startup(function() {
    if (Meteor.settings.public["environment"] !== Constants.ENVIRONMENT.DEVELOPMENT) {
        console.log = () => {};
    }
    
    var getPathAndQueryJson = function (pathAndQuery) {
        var splitPathAndQuery = pathAndQuery.split("?");
        var path = splitPathAndQuery[0];
        var query;
        if (splitPathAndQuery.length > 1) {
            query = splitPathAndQuery[1];
        }
        console.log("path: " + path);
        console.log("query: " + query);
        
        return {path: path, query: query};
    };
    
    var getQueryToken = function (pathAndQueryJson) {
        var token;
        if (pathAndQueryJson.path === Constants.ROUTES.RESET_PASSWORD && pathAndQueryJson.query) {
            token = pathAndQueryJson.query.split("token=")[1];
            console.log("token: " + token);
        }
        return token;
    };
    
    if (Meteor.isCordova) {
        window.handleOpenURL = function (url) {
            console.log("handleOpenURL() received url: " + url);
            var index = url.indexOf(Meteor.settings.public["customUrlScheme"]);
            var isCustomUrlScheme = index > -1;
            console.log("isCustomUrlScheme: " + isCustomUrlScheme);
            if (isCustomUrlScheme) {
                console.log("received custom url: " + url);
                var parts = url.split(Meteor.settings.public["customUrlScheme"]);
                // The path will be the 2nd element
                if (parts.length === 2) {
                    var pathAndQuery = parts[1];
                    console.log("pathAndQuery: " + pathAndQuery);
                    var pathAndQueryJson = getPathAndQueryJson(pathAndQuery);
                    var token = getQueryToken(pathAndQueryJson);
                    Session.set(Constants.SESSION.RESET_PASSWORD_TOKEN, token);
                }
            } else {
                var pathAndQuery = url.split(Meteor.settings.public["host"])[1];
                console.log("pathAndQuery: " + pathAndQuery);
                var pathAndQueryJson = getPathAndQueryJson(pathAndQuery);
                var token = getQueryToken(pathAndQueryJson);
                Session.set(Constants.SESSION.RESET_PASSWORD_TOKEN, token);
            }
        }
    }
});