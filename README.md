# Build
### Requirements
* Node.js
* Meteor
* Mac OSX build host
* Android Studio
* XCode

### Steps
1 Install the dependencies

```
cd app/
npm install
```

2 Increment the version number
    * Edit the `version` property in the `settings.json` of every environment folder in the `config` directory. 
    * Open `app/mobile-config.js` and edit the `version` under `App.info`
3 Build the app

```
npm run build:test // or npm run build:prod
```

* Outputs 
    * `build/windows/server-bundle.tar.gz` 
    * `build/windows/android`
    * `build/windows/ios`
* The build script will copy any changes (i.e. version number) in the `settings.json` to the `web.config` for each environment.  The `config` folder will then be bundled with the server code for easy deployment to any environment.
* The server bundle will be the same regardless of the `build` script used.
* The `build:test` or `build:prod` scripts will set the environment that the mobile app is configure to communicate with along with prebundled settings.

# Server Deployment

* Transfer the `server-bundle.tar.gz` to the GAWEBD01 `C:\Apps\Installation\GA\Portal\Web\PaymentApp\PublicSite`
* Remote desktop to GAWEBD01 and open a file explorer to the location above.
* Stop `Node.js` in the application pool in IIS
* Unzip the bundle
    * Right-click the file > 7-zip > Extrat Here
    * This will produce the file `server-bundle.tar`
* Extract the contents of the bundle
    * Double-click `server-bundle.tar` to open 7-zip
    * Double-click the `bundle` directory to open
    * Extract the contents of the `bundle` directory to the current path.  Should be `C:\Apps\Installation\GA\Portal\Web\PaymentApp\PublicSite\`

* Install the server dependencies

```
cd programs/server/
npm install
```

* Set the web.config
    * Copy `config\development\web.config` to the site root.
    
* Start the `Node.js` application pool in IIS

* Create zip file to be deployed in TEST and PROD
    * Select the following files and folders
        * config
        * programs
        * server
        * node_version.txt
        * main.js
        * README
        * star.json
    * Right-click > 7-zip > Add to "PaymentApp.7z"
    * Rename file to add the version 
        * Example: PaymentApp-v2_X_X_X
        * Version number can be found in the `config/development` in the `settings.json` or `web.config` files
        

### TEST and PROD

* Stop the `Node.js` application pool in IIS
* Open file explorer and navigate to `C:\Apps\Installation\GA\Portal\Web\PaymentApp\PublicSite`
* Open a new file explorer and navigate to `\\gawebd01\Apps\Installation\GA\Portal\Web\PaymentApp\PublicSite`
* Copy the PaymentApp-version bundle that was created.
* Unzip the file
    * Right-click > 7-zip > Extract Here
* Copy the `web.config` from the appropriate environment from the `config` folder
* Start the `Node.js` application pool in IIS
* Verify the deployment
    * Open Google Chrome browser 
        * Navigate to the appropriate URL for the environment being deployed `https://test.gametime.gafundraising.com`
        * Open the "About" page from the navigation menu
        * Verify the version number at the bottom of the page


# Mobile App Deployment
### Android
* Open Android Studio
* Open the project found in `build/windows/android`
    * If no previous project have been opened you will see the "Welcome to Android Studio" dialog.
        * Select "Open an existing Android Studio project"
    * If a previous project was open
        * File > Open
    * Select the `project` folder and hit the "Open" button
        * Android Gradle Plugin Update Recommended
            * Select "Don't remind me again for this project"
* Generate a signed APK
    * Build > Generate Signed APK
    * Module: project > Next
    * Key store path > Choose existing...
        * Navigate to ../app/gametime-keystore.jks
    * Open app/generate-signed-apk.txt for keystore credentials
    * Enter the Key store password
    * Click the "..." next to Key alias
        * Use an existing key: gametimekey > OK
    * Enter the Key password > Next
    * Verify APK Destination Folder
    * Build Type: release > Finish
    * When finished a notification will be displayed indicating the APK was successfully generated and provide a link to open the location of the APK.
* Login to the [Google Play Developer Console](https://developer.android.com/distribute/console/)
* Select GA BLITZ from the "All applications" screen
* From the menu select Release management > App releases
* Start with Alpha testing
    * Click the "Manage" button next to "Alpha" under "Closed track"
    * Click "Create Release" button
    * Drag and drop your `project-release.apk` or click "Browse Files" to select the file.
    * Edit the release name
        * Add the environment the bundle was built for (i.e. v2.5.3.4-TEST)
    * Enter release notes
    * Review
    * Publish to Alpha testing
* Promote to Beta and Prod as needed.
    
### iOS
* Open a "Finder" window
* Navigate to path/to/project/`build/windows/ios/project`
* Open the `BLITZ.xcdoeproj` file
* Select the root "BLITZ" folder in the side panel and make sure you are under the "General" tab
* Edit the bundle Identifier
    * Remove the ".mobile" at the end
        * It should be `com.gafundraising.fiftyniner`
* Verify the version number
* Edit the build number
    * 1.x for Test
    * 2.x for Prod
* Automatically manage signing should be "checked".
* Select the appropriate Team "Southwestern Advantage, Inc."
* Update Release Signing Identity for automatic signing to work
    * Navigate to the Build Settings tab
    * Go to the Signing Section
    * For "Code Signing Identity" for "Release" select "iOS Developer"
* Enable Push Notifications
    * Navigate to the Capabilities tab
        * Turn on "Push Notifications"
    * Right click on the root "BLITZ" folder
        * Add files to "BLITZ"
        * Navigate to `project/BLITZ`
         * Select and Add the following 3 files
            * `BLITZ.entitlements`
            * `Entitlements-Debug.plist`
            * `Entitlements-Release.plist`
    * Select `Entitlements-Debug.plist` in the side menu
        * You should see the `aps-environment` key in the details pane
        * Select and copy the `aps-environment` entry
    * Select the `Entitlements-Release.plist` in the side menu
        * Paste the `aps-environment` entry
        * Change the value from `development` to `production`
* Set the active scheme to "Generic iOS Device" to enable the "Archive" option under "Product"
* From the top menu select Product > Archive
* When the Archive window opens click the "Upload to App Store" button
    * Distribution options
        * Both options should be "checked"
    * Automatically manage signing
    * Review `BLITZ.ipa` content
        * Verify the Certificate and provisioning profile
        * Verify that `aps-environment` is set for `production`
    * Finally Upload the IPA
* Wait for an email from Apple saying that your upload has been processed
* Login to [App Store Connect](https://appstoreconnect.apple.com) to test the new version using Test Flight
