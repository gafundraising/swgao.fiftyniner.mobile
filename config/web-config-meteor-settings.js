var fs = require('fs');
var xml2js = require('../app/node_modules/xml2js');
var parser = new xml2js.Parser();
var _ = require('../app/node_modules/lodash');

var environments = ["development","test","production"];

environments.forEach((environment) => {
    fs.readFile(__dirname + '/' + environment + '/settings.json', function (err, data) {
        if (err) throw err;
        var meteorSettings = JSON.parse(data);
        var meteorSettingsString = JSON.stringify(meteorSettings);

        fs.readFile(__dirname + '/' + environment + '/web.config', function (err, data) {
            if (err) throw err;

            parser.parseString(data, function (err, result) {
                var webConfigJson = result;
                // console.log("result: ", JSON.stringify(result));
                if (webConfigJson.configuration["appSettings"] && webConfigJson.configuration["appSettings"][0]) {
                    var appSettings = webConfigJson.configuration["appSettings"][0]['add'];
                    // Loop through the translated appSettings
                    _.each(appSettings, function (xmlSetting, index) {
                        // Extract the names and values
                        var appSetting = xmlSetting['$'];
                        var settingName = appSetting.key;
                        var settingValue = appSetting.value;
                        if (settingName === "METEOR_SETTINGS") {
                            webConfigJson.configuration["appSettings"][0]['add'][index]['$'].value = meteorSettingsString;
                        }
                    });
                }

                // create a new builder object and then convert
                // our json back to xml.
                var builder = new xml2js.Builder({headless: true});
                var xml = builder.buildObject(webConfigJson);
                // console.log("xml: ", xml);

                fs.writeFile('../config/' + environment + '/web.config', xml, function (err, data) {
                    if (err) console.log(err);
                    console.log("Successfully wrote meteor settings to web.config for " + environment);
                });
            });
        });
    });
});
